<?php
$home = Yii::app()->homeUrl;
$BASE = Yii::app()->baseUrl;
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="language" content="en" />

  <!-- blueprint CSS framework -->
  <!-- link rel="stylesheet" type="text/css" href="<?php echo $BASE; ?>/css/screen.css" media="screen, projection" /-->
  <!-- link rel="stylesheet" type="text/css" href="<?php echo $BASE; ?>/css/print.css" media="print" /-->
  <!--[if lt IE 8]>
  <link rel="stylesheet" type="text/css" href="<?php echo $BASE; ?>/css/ie.css" media="screen, projection" />
  <![endif]-->

  <!--link rel="stylesheet" type="text/css" href="<?php echo $BASE; ?>/css/main.css" /-->
  <!--link rel="stylesheet" type="text/css" href="<?php echo $BASE; ?>/css/form.css" /-->

  <title><?php echo CHtml::encode($this->pageTitle); ?></title>
  <link rel="stylesheet" type="text/css" href="<?php echo $BASE; ?>/css/style.css" media="screen, projection" />
</head>

<body>
<?php
$this->widget('bootstrap.widgets.BootNavbar', array(
    'fixed'=>false,
    'brand'=>Yii::app()->name,
    'brandUrl'=>$home,
    'collapse'=>true, // requires bootstrap-responsive.css
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.BootMenu',
            'items'=>array(
                array('label'=>'Home', 'url'=>array('/site/index')),
                array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
                array('label'=>'Contact', 'url'=>array('/site/contact')),
                array('label'=>'Dropdown', 'url'=>'#', 'items'=>array(
                    array('label'=>'DROPDOWN HEADER', 'itemOptions'=>array('class'=>'nav-header')),
                    array('label'=>'Action', 'url'=>'#'),
                    array('label'=>'Another action', 'url'=>'#'),
                    array('label'=>'Something else here', 'url'=>'#'),
                    '---',
                    array('label'=>'Separated link', 'url'=>'#'),
                )),
            ),
        ),
        '<form class="navbar-search pull-left" action=""><input type="text" class="search-query span2" placeholder="Search"></form>',
        array(
            'class'=>'bootstrap.widgets.BootMenu',
            'htmlOptions'=>array('class'=>'pull-right'),
            'items'=>array(
                array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'Dropdown', 'url'=>'#', 'items'=>array(
                    array('label'=>'Action', 'url'=>'#'),
                    array('label'=>'Another action', 'url'=>'#'),
                    array('label'=>'Something else here', 'url'=>'#'),
                    '---',
                    array('label'=>'Separated link', 'url'=>'#'),
                )),
            ),
        ),
    ),
));
?>
<div class="container" id="page">
<?php
if(isset($this->breadcrumbs))
  $this->widget('bootstrap.widgets.BootCrumb', array(
    'links'=>$this->breadcrumbs,
  ));
echo $content;
?>
  <hr>
  <footer>
    <p>
      &copy;
      <?php echo Yii::app()->params['author'];?>
      <?php echo date('Y'); ?>
    </p>
    <p>
      <?php echo Yii::powered(); ?>
      Less, Bootstrap.
    </p>
  </footer>
</div>
</body>
</html>
